# Ansible automation repository

This repository will hold relevant automation playbooks and roles that will be used in automation tasks (image generation, app deployment, etc).

## Requirements
* Install ansible: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## Usage
To be able to use the ansible automation, clone the repository:
* git clone _repo url_ ~/ansible

## Basic folder structure
According to Ansible best-practices, this repository's folder structure should follow this layout (playbook and role names are examples):

Ref.: https://docs.ansible.com/ansible/2.8/user_guide/playbooks_best_practices.html

```
odoo.yml                  # playbook for service/application
postgresql.yml            # playbook for service/application
test_playook1.yml         # test playbook for automation

roles/
    common/               # this hierarchy represents a "role"
        tasks/            #
            main.yml      #  <-- tasks file can include smaller files if warranted
        handlers/         #
            main.yml      #  <-- handlers file
        templates/        #  <-- files for use with the template resource
            ntp.conf.j2   #  <------- templates end in .j2
        files/            #
            bar.txt       #  <-- files for use with the copy resource
            foo.sh        #  <-- script files for use with the script resource
        vars/             #
            main.yml      #  <-- variables associated with this role
        defaults/         #
            main.yml      #  <-- default lower priority variables for this role
        meta/             #
            main.yml      #  <-- role dependencies
        library/          # roles can also include custom modules
        module_utils/     # roles can also include custom module_utils
        lookup_plugins/   # or other types of plugins, like lookup in this case

    odoo/                 # same kind of structure as "common" was above, done for the webtier role
    postgresql/           # ""
```

## Adding functionality
To add new roles or playbooks (or any new code or change to this repository, begin by evaluating if there are new dependencies and generate requirements.txt accordigly:

* pip install pip-tools (if already installed, skip this step)
* edit requirements.in file with needed pip packages, one per line;
* pip-tools compile requirements.in

### Requirements
To try and ensure general usage rules, please execute the following instructions, to help you in the commit process:

Ref.: https://pre-commit.com/
```
pip install -r requirements.txt
pre-commit install
pre-commit install --hook-type commit-msg
```

### Guidelines
* This repo does not allow direct push to the main branch! Always use feature branches, push the code to them and open a Merge Request (or Pull Request) for evaluation;
* Follow the conventional commits syntax for commits (https://www.conventionalcommits.org/en/v1.0.0/). This will not only make the commit messages more uniformly readable, but will also enable automatic git tagging/versioning, automatic change log generation, etc...
* Do not bypass hooks when commiting code! They are there to try and make everyone's life easier. If you don't like or agree with what is being proposed, communicate that with the team and we will surely come to an agreement.
* **Always remember: You work with team mates!** Please follow the agreed guidelines so that everyone has an easier time helping you in your work, and you helping with theirs! :)
